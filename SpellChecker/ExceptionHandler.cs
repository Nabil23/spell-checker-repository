﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpellChecker
{
    public class ExceptionHandler
    {
        public static void CheckStringMaxLength(string word, int maxLength)
        {
            if (word.Length > maxLength)
                throw new Exception("Length of the string should not exceed max of " + maxLength);   
        }
    }
}
