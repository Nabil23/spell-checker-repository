﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpellChecker
{
    internal interface ISpellChecker
    {
        void GetResult();
    }
}
