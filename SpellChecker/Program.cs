﻿using System;

namespace SpellChecker
{
    class Program
    {
        static void Main(string[] args)
        {
            ISpellChecker spellChecker = new SpellChecker();
            spellChecker.GetResult(); 
        }
    }
}
