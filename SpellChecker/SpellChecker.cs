﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpellChecker
{
    internal class SpellChecker : ISpellChecker
    {
        private const string InputExample = "rain spain plain plaint pain main mainly the in on fall falls his was";
        private const string TextExample = "hte rame in pain fells mainy oon teh lain was hints pliant";
        private string Input { get; set; }
        private List<string> Dictionary { get; set; }
        public SpellChecker()
        {
            Dictionary = new List<string>();
        }

        public void GetResult()
        {
            Console.WriteLine("Welcome to spell checker application! Please enter your dictionary!");
            SetInput();
        }

        private void SetInput()
        {
            // rain spain plain plaint pain main mainly the in on fall falls his was
            Console.WriteLine();
            Console.WriteLine("Input");
            Input = InputExample;// Console.ReadLine();
            ExceptionHandler.CheckStringMaxLength(Input, 100); // 50 Exception

            Console.WriteLine(Input);

            FillDictionary();

            var filledText = TextExample;// GetFilledText();
            ExceptionHandler.CheckStringMaxLength(filledText, 100); // 50 Exception
            Console.WriteLine();
            Console.WriteLine("Filled text");
            Console.WriteLine("===");
            Console.WriteLine(filledText);
            Console.WriteLine("===");
            Console.WriteLine();
            Console.WriteLine("Result");
            Console.WriteLine(GetOutput(filledText));
        }

        private string GetOutput(string text)
        {
            var builderOutput = new StringBuilder();
            var builder = new StringBuilder();
            foreach (var inp in text)
            {
                if ((inp < 65 || inp > 90) && (inp < 97 || inp > 122)) // 65 - A, 90 - Z, 97 - a, 122 - z
                {
                    builderOutput.Append(GetAccumulatedOutput(builder.ToString()) + inp);
                    builder.Clear();

                    continue;
                }

                builder.Append(inp);
            }

            builderOutput.Append(GetAccumulatedOutput(builder.ToString()));

            return builderOutput.ToString();
        }
        private string GetAccumulatedOutput(string word)
        {
            var output = string.Empty;
            if (Dictionary.Contains(word))
            {
                output = word;
                return output;
            }

            if (word != GetComparedWithSameRoot(word))
            {
                output = GetComparedWithSameRoot(word);
                return output;
            }

            if (word != GetComparedWithSameCharatersWord(word))
            {
                output = GetComparedWithSameCharatersWord(word);
                return output;
            }

            if (word != GetComparedWithOneCharacterDifference(word))
            {
                output = GetComparedWithOneCharacterDifference(word);
                return output;
            }

            output += "{" + word + "?" + "}";
            return output;
        }
        private string GetComparedWithSameRoot(string word)
        {
            var possibleWords = new List<string>();
            int countOfDistinction;
            bool rootFlag;
            foreach (var dictWord in Dictionary)
            {
                countOfDistinction = 0;
                rootFlag = false;
                var characterCountDifference = Math.Abs(word.Length - dictWord.Length);
                if (characterCountDifference != 1)
                    continue;

                for (var i = 0; i < word.Length; i++)
                {
                    if (i > dictWord.Length - 1)
                        continue;

                    if (i == 0 && dictWord[i] != word[i])
                    {
                        rootFlag = true;
                        break;
                    }

                    if (dictWord[i] == word[i])
                        continue;

                    if (dictWord[i] != word[i] && i != word.Length - 1)
                    {
                        rootFlag = true;
                        break;
                    }

                    ++countOfDistinction;
                }

                if (rootFlag)
                    continue;

                if (countOfDistinction < 2)
                {
                    possibleWords.Add(dictWord);
                    countOfDistinction = 0;

                    continue;
                }
            }

            var result = GetOutputFromWord(word, possibleWords);
            return result;
        } // 1
        private string GetComparedWithSameCharatersWord(string word)
        {
            var possibleWords = new List<string>();

            bool flag;
            foreach (var dictWord in Dictionary)
            {
                flag = false;

                var characterCountDifference = Math.Abs(word.Length - dictWord.Length);
                if (characterCountDifference > 1)
                    continue;

                foreach (var ch in word)
                {
                    if (dictWord.Contains(ch))
                        continue;

                    flag = true;
                    break;
                }

                if (flag)
                    continue;

                possibleWords.Add(dictWord);
            }

            var result = GetOutputFromWord(word, possibleWords);
            return result;
        } // 2
        private string GetComparedWithOneCharacterDifference(string word)
        {
            int countOfDistinction;
            var possibleWords = new List<string>();

            foreach (var dictWord in Dictionary)
            {
                countOfDistinction = 0;
                var characterCountDifference = Math.Abs(word.Length - dictWord.Length);
                if (characterCountDifference > 1)
                    continue;

                foreach (var ch in word)
                {
                    if (dictWord.Contains(ch))
                        continue;

                    ++countOfDistinction;
                }

                if(countOfDistinction < 2)
                {
                    possibleWords.Add(dictWord);
                    countOfDistinction = 0;

                    continue;
                }
            }

            var result = GetOutputFromWord(word, possibleWords);
            return result;
        } // 3
        private string GetFilledText()
        {
            var text = Console.ReadLine();
            return text;
        }
        private void FillDictionary()
        {
            var builder = new StringBuilder();
            foreach (var inp in Input)
            {
                if ((inp < 65 || inp > 90) && (inp < 97 || inp > 122)) // 65 - A, 90 - Z, 97 - a, 122 - z
                {
                    FillBuiderIfNotEmpty(builder);
                    continue;
                }

                builder.Append(inp);
            }

            FillBuiderIfNotEmpty(builder);
        }
        private void FillBuiderIfNotEmpty(StringBuilder builder)
        {
            if (builder.Length != 0)
            {
                var word = builder.ToString();
                if (!Dictionary.Contains(word))
                    Dictionary.Add(word);

                builder.Clear();
            }
        }
        private string GetOutputFromWord(string word, List<string> possibleWords)
        {
            var builder = new StringBuilder();

            if (!possibleWords.Any())
                return word;
            else if (possibleWords.Count == 1)
                return possibleWords.FirstOrDefault();

            builder.Append("{ ");
            for (var i = 0; i < possibleWords.Count; i++)
            {
                builder.Append(possibleWords[i]);
                if (i != possibleWords.Count - 1)
                    builder.Append(" ");
            }

            builder.Append(" }");
            return builder.ToString();
        }
    }
}
